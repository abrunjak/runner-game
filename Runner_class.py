import pygame
from sys import exit
from random import randint
from Score import Score

class Runner:
    def __init__(self):
        pygame.init()
        pygame.display.set_caption("Runner")
        self.screen = pygame.display.set_mode((800,400))
        self.clock = pygame.time.Clock()
        self.font = pygame.font.Font(None, 50)
        self.score_font = pygame.font.Font(None, 30)
        self.game_actve = False
        self.player = False
        self.start_time = 0
        self.score = 0

        self.sky_surface = pygame.image.load("graphics/Sky.png").convert()
        self.ground_surface = pygame.image.load("graphics/ground.png").convert()

        self.stone_surf = pygame.image.load("graphics/stone.png").convert_alpha()
        self.bird_surf = pygame.image.load("graphics/bird.png").convert_alpha()

        self.obstacle_rect_list = []

        self.player_name = ""
        self.player_input = self.font.render("Enter your name:", True, "Black")
        self.player_input_rect = self.player_input.get_rect(center = (400, 180))
        self.player_input_box = pygame.Rect(250, 200, 300, 50)

        self.player_surf = pygame.image.load("graphics/player.png").convert_alpha()
        self.player_rect = self.player_surf.get_rect(midbottom = (80,300))
        self.player_gravity = 0

        self.game_name = self.font.render("Runner", False, "Yellow")
        self.game_name_rect = self.game_name.get_rect(center = (400, 60))

        self.game_message = self.score_font.render("Press SPACE to continue", False, "Yellow")
        self.game_message_rect = self.game_message.get_rect(center = (670, 80))

        self.game_message1 = self.score_font.render("Press SPACE to jump", False, "Black")
        self.game_message1_rect = self.game_message1.get_rect(center = (670, 80))

        self.game_message2 = self.score_font.render("Press ENTER to continue", False, "Black")
        self.game_message2_rect = self.game_message2.get_rect(center = (670, 80))

        self.obstacle_timer = pygame.USEREVENT + 1
        pygame.time.set_timer(self.obstacle_timer, 1500)

    def display_update(self):
        pygame.display.update()
        self.clock.tick(60)

    def display_score(self):
        currnet_time = int(pygame.time.get_ticks()/1000) - self.start_time
        score_surf = self.font.render(f"Score: {currnet_time}", False, "Black")
        score_rect = score_surf.get_rect(center = (400,50))
        self.screen.blit(score_surf, score_rect)
        return currnet_time

    def obstacle_movement(self, obstacle_list):
        if obstacle_list:
            for obstacle_rect in obstacle_list:
                obstacle_rect.x -= 5
                if obstacle_rect.bottom == 300:
                    self.screen.blit(self.stone_surf, obstacle_rect)   
                else:
                    self.screen.blit(self.bird_surf, obstacle_rect)
            obstacle_list = [obstacle for obstacle in obstacle_list if obstacle.x > -100]
            return obstacle_list
        else:
            return []

    def collisions(self, player, obstacles):
        if obstacles:
            for obstacle_rect in obstacles:
                if player.colliderect(obstacle_rect):
                    Score.addScore("RunnerScore.db", self.player_name, self.score)
                    return False
        return True



    def events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if self.game_actve and self.player:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE and self.player_rect.bottom == 300:
                        self.player_gravity = -20

            elif not self.player and not self.game_actve:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RETURN:
                        if len(self.player_name.strip()) > 0:
                            self.player = True
                            self.game_actve = True
                            self.start_time = int(pygame.time.get_ticks()/1000)
                        
                    elif event.key == pygame.K_BACKSPACE:
                        self.player_name = self.player_name[:-1]
                    else:
                        if len(self.player_name) <= 10:
                            self.player_name += event.unicode
            else:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE: 
                        self.game_actve = False
                        self.player = False
                        self.player_name = ""

            if event.type == self.obstacle_timer and self.game_actve:
                if randint(0, 2):
                    self.obstacle_rect_list.append(self.stone_surf.get_rect(bottomright = (randint(900,1100), 300)))
                else:
                    self.obstacle_rect_list.append(self.bird_surf.get_rect(bottomright = (randint(900,1100), 210)))

    def nameInput(self):
        self.screen.fill("Cyan")
        txt_surface = self.font.render(self.player_name, True, (0, 0, 0))
        self.screen.blit(self.game_name, self.game_name_rect)
        self.screen.blit(self.player_input, self.player_input_rect)
        self.screen.blit(txt_surface, (self.player_input_box.x+5, self.player_input_box.y+5))
        if len(self.player_name.strip()) > 0:
            self.screen.blit(self.game_message2, self.game_message2_rect)
        pygame.draw.rect(self.screen, (0, 0, 0), self.player_input_box, 2)

    def game(self):
        self.screen.blit(self.sky_surface, (0,0))
        self.screen.blit(self.ground_surface, (0,300))
        self.screen.blit(self.game_message1, self.game_message1_rect)
        self.score = self.display_score()

        self.player_gravity+=1
        self.player_rect.y += self.player_gravity
        if self.player_rect.bottom >= 300:
            self.player_rect.bottom = 300
        self.screen.blit(self.player_surf, self.player_rect)

        self.obstacle_rect_list = self.obstacle_movement(self.obstacle_rect_list)

        self.game_actve = self.collisions(self.player_rect, self.obstacle_rect_list)

    def scores(self):
        self.screen.fill("Cyan")
        self.obstacle_rect_list.clear()
        self.player_rect.midbottom = (80,300)
        self.player_gravity = 0
        scores = Score.showScore("RunnerScore.db")
        self.screen.blit(self.game_name, self.game_name_rect)
        self.screen.blit(self.game_message, self.game_message_rect)
        for key in scores.keys():
            sc = scores[key]
            score_message1 = self.score_font.render(str(key), True, "Black")
            self.screen.blit(score_message1, (100,80+(key-1)*32))
            score_message2 = self.score_font.render(sc["Name"], True, "Black")
            self.screen.blit(score_message2, (140,80+(key-1)*32))
            score_message3 = self.score_font.render(str(sc["Score"]), True, "Black")
            self.screen.blit(score_message3, (280,80+(key-1)*32))
