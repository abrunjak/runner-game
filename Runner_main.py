from Runner_class import Runner

game = Runner()

while True:
    game.events()
    if not game.player and not game.game_actve:
        game.nameInput()
    elif game.game_actve and game.player:
        game.game()
    else:
        game.scores()
    game.display_update()