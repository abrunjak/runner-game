import sqlite3

class Score:
    def addScore(datapath, name, score):
        with sqlite3.connect(datapath) as conn:
            cursor = conn.cursor()
            cursor.execute("CREATE TABLE IF NOT EXISTS RunnerScore(Game INTEGER PRIMARY KEY AUTOINCREMENT, Name, Score)")
            cursor.execute(f"INSERT INTO RunnerScore(Name, Score) VALUES('{name}', {score})")
            conn.commit()
    
    def showScore(datapath):
        score = {}
        with sqlite3.connect(datapath) as conn:
            cursor = conn.cursor()
            cursor.execute("CREATE TABLE IF NOT EXISTS RunnerScore(Game INTEGER PRIMARY KEY AUTOINCREMENT, Name, Score)")
            cursor.execute("SELECT * FROM RunnerScore ORDER BY Score DESC")
            all_scores = cursor.fetchall()
            row = 1
            for sc in all_scores:
                if row > 10:
                    break
                score[row] = {"Name": sc[1], "Score": sc[2]}
                row += 1 
        return score